const express = require('express')
const app = express()
const cors = require('cors')
require('dotenv').config()

const router = require('./routers.js')

const PORT = process.env.PORT || 8888;
const HOST = process.env.YOUR_HOST || '0.0.0.0'

app.use(cors({origin: '*'}))
app.use(express.json())
app.use('/photos',express.static('albums'))


app.get('/', (req, res) => {
  res.status(200).json({
    "message": "OK"
  })
})
app.use(router)

app.listen(PORT, HOST,() => {
  console.log(`Listening on port ${PORT} in ${HOST}`);
})