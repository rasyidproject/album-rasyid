const router = require('express').Router()
const Album = require('./controllers/AlbumController')
const Uploader = require('./middleware/Uploader')

router.post('/photos/list', Album.getAllPhotos)
router.put('/photos', Uploader.array('documents'), Album.uploadPhotos)
router.delete('/photos/:album/:filename', Album.deletePhoto)
router.delete('/photos', Album.deleteMultiplePhotos)

module.exports = router