const multer = require('multer')
const path = require('path')

module.exports = {
  development: {
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(
          null,
          path.resolve(__dirname, '..', 'albums',req.body.album)
          )
      },
      filename: (req, file, cb) => {
        const split = file.originalname.split('.')
        const extension = split[split.length - 1]
        const name = file.fieldname + '-' + Date.now() + '.' + extension
        const category = (req.body.album).toLowerCase()

        file.url = `${process.env.BASE_URL}/photos/${category}/${name}`
        file.relative_path = `albums/${category}/${name}`
        file.name = name
        file.album = req.body.album
        cb(null, name)
      }
    })
  },
  test: {},
  production: {}
}