const album = require('../models/album')

module.exports = {
  getAllPhotos: (req,res) => {

    let albumDirectory = './albums'
    let fs = require('fs')
    let folder = fs.readdirSync(albumDirectory)
    let data = []
    let faker = require('faker')
    try {
      folder.forEach(category => {
        if(category !== 'placeholder'){

          // Change category string into camelcase
          category = category.split('')
          category[0] = category[0].toUpperCase()
          category = category.join('')

          let file = fs.readdirSync(`${albumDirectory}/${category}`)
          
          file.forEach(name => {
            data.push({
              id: faker.random.uuid(),
              album: category,
              name: name,
              path: `albums/${category}/${name}`,
              raw: `${process.env.BASE_URL}/photos/${category}/${name}`
            })
          })
        }
      })

      // Pagination
      let list = []
      let {skip,limit} = req.body
      if(limit > data.length)
        limit = data.length
      for (let index = skip; index < limit; index++) {
        list.push(data[index])
      }

      res.status(200).json({
        message: 'OK',
        documents: list
      })
    } catch (error) {
      res.status(400).json({
        message: 'FAIL',
        errors: error.message
      })
    }
  },
  uploadPhotos: (req,res) => {
    try {
      let data = []
      req.files.forEach(file => {
        data.push({
          album: file.album,
          name: file.name,
          path: file.relative_path,
          raw: file.url
        })
      })
      res.status(200).json({
        message: 'OK',
        data: data
      })
    } catch (error) {
      res.status(400).json({
        message: 'FAIL',
        errors: error.message
      })
    }
  },
  deletePhoto: (req,res) => {
    try {
      const {album,filename} = req.params
      const fs = require('fs')
      const path = `./albums/${album}/${filename}`
      if(fs.existsSync(path))
        fs.unlinkSync(path)
      else
        console.log('file doesn\'t exist')
      res.status(200).json({
        message: 'OK'
      })
    } catch (error) {
      res.status(400).json({
        message: 'FAIL'
      })
    }
  },
  deleteMultiplePhotos: (req,res) => {
    try {
      const fs = require('fs')

      req.body.forEach(doc => {
        let split = doc.documents.split(',')
        split.forEach(file =>{
          const path = `./albums/${doc.album}/${file}`
          console.log(path)
          if(fs.existsSync(path))
            fs.unlinkSync(path)
          else
            console.log('file doesn\'t exist')
        })
      })
      res.status(200).json({
        message: 'OK'
      })
    } catch (error) {
      res.status(400).json({
        message: 'FAIL'
      })
    }
  }
}